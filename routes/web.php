<?php

use App\Http\Controllers\StoreDataController;
use App\Models\User;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/config-cache', function () {
    return Artisan::call('config:cache');
});
Route::get('/', function () {
    return view('welcome');
});
Route::post('/store-data', [StoreDataController::class,'store'])->name('store.data');
Route::get('/get-address', [StoreDataController::class,'getAddress'])->name('get.address');
Route::get('/get-words', [StoreDataController::class,'getWords'])->name('get.words');
Route::get('/convert-video/{user}', [StoreDataController::class,'reduceVideoSize']);

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/migrate-fresh', function() {
    Artisan::call('migrate:fresh --seed');
    echo 'dump-autoload complete';
});
