<div>
    <p>Id: {{ $data['id'] }}</p>
    <p>Date: {{ $data['date'] }}</p>
    <p>Address : {{ $data['address'] }}</p>
    <p>Words : {{ $data['words'] }}</p>
    <p>Email: {{ $data['email'] }}</p>
    <p>File: <a href="{{ $data['file_url'] }}">{{ $data['file_url'] }}</a></p>
    <p>latitude: {{ $data['latitude'] }}</p>
    <p>longitude: {{ $data['longitude'] }}</p>
</div>
