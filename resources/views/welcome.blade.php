<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/css/@fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets/css/iziToast.min.css') }}">
    <link href="{{ asset('assets/css/sweetalert.css') }}" rel="stylesheet" type="text/css"/>
    <!-- Bootstrap 4.1.1 -->
    <style>
        .button-file input {
            visibility: hidden;
            height: 0;
            width: 0;
        }

        .address {
            max-width: 320px;
            display: block;
        }

        .file-container {
            display: flex;
            justify-content: center;
            flex-direction: column;
            align-items: center;
        }

        .file-container label {
            width: fit-content;
        }

        .img-preview {
            max-height: 100%;
            max-width: 100%;
            object-fit: cover;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row  justify-content-center">
        <div class="col-lg-8 col-sm-12 col-12">
            <div class="card shadow rounded mt-lg-5 mt-2">
                <div class="card-header">
                    <div class="card-title text-center">
                        <h2 class="mb-0">Leakloc</h2>
                    </div>
                </div>
                <div class="card-body">
                    <form class="mt-5" action="{{ route('store.data') }}" method="POST" id="form"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-group file-container ">
                            <label class="btn btn-primary button-file">
                                Take Photo or Video
                                <i class="fas fa-camera"></i>
                                <input type="file" id="file" accept="video/*,image/*" style="" capture="camera"
                                       name="file">
                            </label>
                            <img src="" class="img-preview my-3" style="display: none" id="imgPreview" alt="">
                            <video width="320" class="my-3" controls style="display: none">
                                <source src=""  id="videPreview">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>

                        <div class="form-group">
                            <label for="email">Email <small>(optional)</small></label>
                            <input type="email" id="email" name="email" class="form-control">
                        </div>

                        <label class="font-weight-bold">latitude:</label>
                        <label id="latitude"></label>
                        <br/>

                        <label class="font-weight-bold">longitude:</label>
                        <label id="longitude"></label>
                        <br/>

                        <label class="font-weight-bold">Words:</label>
                        <label id="words"></label>
                        <br/>

                        <label class="font-weight-bold">Address:</label>
                        <label id="address" class="address"></label>

                        <div class="text-center mt-3">
                            <button class="btn btn-primary" id="btnSave"
                                    data-loading-text="<span class='spinner-border spinner-border-sm'></span> Processing...">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/popper.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert.min.js') }}"></script>
<script src="{{ asset('assets/js/iziToast.min.js') }}"></script>
<script>
    let latitude, longitude, address,words;
    (function ($) {
        $.fn.button = function (action) {
            if (action === 'loading' && this.data('loading-text')) {
                this.data('original-text', this.html()).html(this.data('loading-text')).prop('disabled', true);
            }
            if (action === 'reset' && this.data('original-text')) {
                this.html(this.data('original-text')).prop('disabled', false);
            }
        };
    }(jQuery));

    const successfulLookup = position => {
        latitude = position.coords.latitude;
        longitude = position.coords.longitude;
        $('#latitude').text(latitude);
        $('#longitude').text(longitude);
        $.ajax({
            url: '{{route('get.address')}}',
            data: {latitude, longitude},
            success: function (result) {
                if (result.success) {
                    address = result.data;
                    $('#address').text(address);
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
        });
        $.ajax({
            url: '{{route('get.words')}}',
            data: {latitude, longitude},
            success: function (result) {
                if (result.success) {
                    words = result.data;
                    $('#words').text(words);
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
        });
    }
    if (window.navigator.geolocation) {
        window.navigator.geolocation.getCurrentPosition(successfulLookup, console.log);
    } else {
        alert('Geolocation is not supported by this browser.');
        $('#btnSave').attr('disabled', true);
    }
    $(document).on('change', '#file', function (e) {
        if (isValidFile('#file')) {
            displayFile(this);
        }
    });

    $(document).on('submit', '#form', function (e) {
        e.preventDefault();
        let fileName = $('#file').val();
        if (fileName === '') {
            displayErrorMessage('file required')
            return;
        }
        let formData = new FormData($('#form')[0]);
        formData.append('latitude', latitude);
        formData.append('longitude', longitude);
        formData.append('address', address);
        formData.append('words', words);
        processingBtn('#form', '#btnSave', 'loading');
        $.ajax({
            url: $(this).attr('action'),
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                if (result.success) {
                    displaySuccessMessage(result.message);
                    setTimeout(function () {
                        location.reload();
                    }, 1500);
                }
            },
            error: function (result) {
                displayErrorMessage(result.responseJSON.message);
            },
            complete: function () {
                processingBtn('#form', '#btnSave');
            },
        });
    });

    function displaySuccessMessage(message) {
        iziToast.success({
            title: 'Success',
            message: message,
            position: 'topRight'
        });
    }

    function displayErrorMessage(message) {
        iziToast.error({
            title: 'Error',
            message: message,
            position: 'topRight'
        });
    }

    function processingBtn(selecter, btnId) {
        let state = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;
        let loadingButton = $(selecter).find(btnId);

        if (state === 'loading') {
            loadingButton.button('loading');
        } else {
            loadingButton.button('reset');
        }
    }

    function isValidFile(inputSelector) {
        let ext = $(inputSelector).val().split('.').pop().toLowerCase();
        if ($.inArray(ext, ['mp4', 'png', 'jpg', 'jpeg']) == -1) {
            $(inputSelector).val('');
            displayErrorMessage('The image must be a file of type: mp4,jpeg, jpg, png.');
            return false;
        }
        return true;
    }

    function displayFile(input) {
        $('#videPreview').parent().hide();
        $('#imgPreview').hide();
        let ext = $(input).val().split('.').pop().toLowerCase();
        if (ext === 'mp4') {
            $('#videPreview')[0].src = URL.createObjectURL(input.files[0]);
            $('#videPreview').parent().show();
            $('#videPreview').parent()[0].load();
        } else {

            let displayPreview = true;
            if (input.files && input.files[0]) {
                let reader = new FileReader();
                reader.onload = function (e) {
                    let image = new Image();
                    image.src = e.target.result;
                    image.onload = function () {
                        $('#imgPreview').attr('src', e.target.result);
                        displayPreview = true;
                    };
                };
                if (displayPreview) {
                    reader.readAsDataURL(input.files[0]);
                    $('#imgPreview').show();
                }
            }
        }
    }

</script>
</html>
