<?php

namespace App\Http\Controllers;

use App\Mail\UserInfo;
use App\Models\User;
use Exception;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Filters\Video\VideoFilters;
use FFMpeg\Format\Video\X264;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use ProtoneMedia\LaravelFFMpeg\Support\FFMpeg;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;

class StoreDataController extends AppBaseController
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(Request $request): JsonResponse
    {
        $input = $request->all();
        try {
            DB::beginTransaction();

            $location = json_encode(Arr::only($input, ['latitude', 'longitude']));

            $user = User::create([
                'location' => $location,
                'address' => $input['address'],
                'email' => $input['email'],
                'words' => $input['words'],
            ]);

            $media = $user->addMedia($input['file'])->toMediaCollection(User::FILE_PATH, config('app.media_disc'));

            $fileUrl = $media->getFullUrl();
            $user->update(['file_url' => $fileUrl]);
            if($media->mime_type == 'video/mp4'){
                $this->reduceVideoSize($user);
            }
//            Mail::to('sendmail@geo-locate.co.uk')->send(new UserInfo($user));

            DB::commit();
            return $this->sendSuccess('Data Saved Successfully');
        } catch (Exception $e) {
            DB::rollBack();

            throw new UnprocessableEntityHttpException($e->getMessage());
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getAddress(Request $request): JsonResponse
    {
        $input = $request->all();
        $key = config('app.google_map_key');
        $latitude = trim($input['latitude']);
        $longitude = trim($input['longitude']);
        $response = Http::get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $latitude . ',' . $longitude . '&sensor=false&key=' . $key);
        $body = $response->json();

        if (count($body['results']) > 0) {
            $address = $body['results'][0]['formatted_address'];
            return $this->sendResponse($address, '');
        }

        return $this->sendError($body['error_message']);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getWords(Request $request): JsonResponse
    {
        $input = $request->all();
        $key = config('app.what_3_words_key');
        $latitude = trim($input['latitude']);
        $longitude = trim($input['longitude']);
        $response = Http::get('https://api.what3words.com/v3/convert-to-3wa?coordinates=' . $latitude . ',' . $longitude . '&key=' . $key);
        $body = $response->json();

        if (!isset($body['error'])) {
            $words = $body['words'];
            return $this->sendResponse($words, '');
        }

        return $this->sendError($body['error']['message']);
    }


    public function reduceVideoSize(User $user)
    {
        $media = $user->media->first();
        $disk = $media->disk;
        $path = $user->id . '/' . $media->file_name;
        $lowBitrateFormat = (new X264('libmp3lame', 'libx264'))->setKiloBitrate(500);
        $start = TimeCode::fromSeconds(0);
        $duration = TimeCode::fromSeconds(10);
        $tempPath = 'converted/' . $path;
        FFMpeg::fromDisk($disk)
            ->open($path)
            ->addFilter(function (VideoFilters $filters) use ($duration, $start) {
                $filters->resize(new Dimension(200, 200));
                $filters->clip($start, $duration);
            })
            ->export()
            ->toDisk($disk)
            ->inFormat($lowBitrateFormat)
            ->save($tempPath);
        Storage::disk($disk)->delete($path);
        Storage::disk($disk)->move($tempPath, $path);
        Storage::disk($disk)->deleteDirectory('converted');
    }
}
