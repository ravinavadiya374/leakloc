<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserInfo extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var User
     */
    protected $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): UserInfo
    {
        $data['address'] = $this->user->address;
        $data['email'] = $this->user->email;
        $data['file_url'] = $this->user->file_url;
        $location = json_decode($this->user->location);
        $data['latitude'] = $location->latitude;
        $data['longitude'] = $location->longitude;
        $data['id'] = $this->user->id;
        $data['date'] = $this->user->created_at->toDateTimeLocalString();
        $data['words'] = $this->user->words;

        return $this->view('user_info',compact('data'));
    }
}
